# PYLOTE

* **Site Web :** http://pascal.peter.free.fr/pylote.html
* **Email :** pascal.peter at free.fr
* **Licence :** GNU General Public License (version 3)
* **Copyright :** (c) 2008-2019

----

Pylote est un logiciel permettant de dessiner sur l'écran de l'ordinateur, ainsi que de manipuler différents instruments de géométrie.  
C’est un logiciel multiplateforme, libre (licence GNU GPL), fait en Python (langage de programmation) et PyQt pour l'interface graphique.  
Pylote fonctionne avec une photo d'écran de l'ordinateur.  
Sur cette image, on peut dessiner et manipuler des instruments de géométrie.

----

## DÉPENDANCES
* [Python](https://www.python.org) : langage de programmation
* [Qt](https://www.qt.io) : "toolkit" très complet (interface graphique et tout un tas de choses)
* [PyQt](https://riverbankcomputing.com) : lien entre Python et Qt

#### Autres bibliothèques utilisées et trucs divers
* [marked](https://github.com/chjj/marked) : pour afficher les fichiers Markdown
* [Breeze Icons](https://api.kde.org/frameworks/breeze-icons/html/index.html)

----

## Les auteurs de PYLOTE

#### Programmation :
* Pascal Peter (2008-2019)

#### Traduction :
* Français : Pascal Peter
* Hongrois : Róbert Somogyvári (cr04ch at gmail.com)

#### Divers :
* Sylvain Bignon ([http://cdpmaths.free.fr](http://cdpmaths.free.fr))  
qui m'a beaucoup aidé à améliorer ce programme via le forum Ubuntu-fr,  
a réalisé plusieurs images utilisées par Pylote  
et est l'auteur d'une bonne partie des fonds d'écran.
