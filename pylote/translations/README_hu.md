# PYLOTE

* **Site Web :** http://pascal.peter.free.fr/pylote.html
* **Email :** pascal.peter at free.fr
* **Licence :** GNU General Public License (version 3)
* **Copyright :** (c) 2008-2019

----

A Pylote segítségével rajzolhatsz és különféle geometriai eszközöket használhatsz a számítógép képernyőjén.  
A Pylote egy multiplatformos, GNU GPL licencű szabad szoftver. Python programozási nyelven, a GUI PyQt-ben készült.  
A Pylote a számítógép képernyőjének egy fotóját tölti be. Ezen a képen tudsz azután a geometriai és rajzoló eszközöket használni.

----

## KÖVETELMÉNYEK
* [Python](https://www.python.org)
* [Qt](https://www.qt.io)
* [PyQt](https://riverbankcomputing.com)

#### Other libraries used and other stuff:
* [marked](https://github.com/chjj/marked)
* [Breeze Icons](https://api.kde.org/frameworks/breeze-icons/html/index.html)

----

## Authors of PYLOTE

#### Programming:
* Pascal Peter (2008-2019)

#### Localization:
* French : Pascal Peter
* Hungarian : Róbert Somogyvári (cr04ch at gmail.com)

#### Other:
* Sylvain Bignon ([http://cdpmaths.free.fr](http://cdpmaths.free.fr))  
who helped me to improve this program via the forum Ubuntu-fr,  
made several images used by Pylote  
and is the author of much of wallpapers.
