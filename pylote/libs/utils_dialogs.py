# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------
# This is a part of Pylote project.
# Author:       Pascal Peter
# Copyright:    (C) 2008-2019 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------


"""
DESCRIPTION :
    Les fenêtres d'outils (toolbars) et autres dialogs utiles.
"""

# importation des modules utiles :
from __future__ import division, print_function
import sys
import os

import utils, utils_functions, utils_instruments

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



###########################################################"
#   DIALOG DE CRÉATION D'UN TEXTE
###########################################################"

class TextItemDlg(QtWidgets.QDialog):
    """
    La fenêtre de dialogue de création d'un texte.
    Le texte s'affiche dans la police sélectionnée, dans un QTextEdit.
    """
    def __init__(self, parent=None, graphicsTextItem=None):
        super(TextItemDlg, self).__init__(parent)

        self.graphicsTextItem = graphicsTextItem
        self.parent = parent

        self.editor = QtWidgets.QTextEdit()
        self.editor.setAcceptRichText(False)
        self.editor.setTabChangesFocus(True)

        # le bouton de police d'écriture :
        chooseFontButton = QtWidgets.QPushButton(
            utils.doIcon('preferences-desktop-font'), '')
        chooseFontButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'Font'))
        chooseFontButton.clicked.connect(self.chooseFont)
        # le bouton de couleur :
        penColorButton = QtWidgets.QPushButton(
            utils.doIcon('format-text-color'),  '')
        penColorButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'Color'))
        penColorButton.clicked.connect(self.penColor)

        self.buttonBox = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok | 
            QtWidgets.QDialogButtonBox.Cancel)
        self.buttonBox.button(
            QtWidgets.QDialogButtonBox.Ok).setEnabled(False)

        if self.graphicsTextItem is not None:
            self.font = self.graphicsTextItem.font()
            self.color = self.graphicsTextItem.defaultTextColor()
            self.editor.document().setDefaultFont(self.font)
            self.editor.setTextColor(self.color)
            self.editor.setPlainText(
                self.graphicsTextItem.toPlainText())
        else:
            self.font = self.parent.font
            self.color = self.parent.fontColor

        layout = QtWidgets.QGridLayout()
        layout.addWidget(self.editor, 1, 0, 1, 6)
        layout.addWidget(chooseFontButton, 2, 0)
        layout.addWidget(penColorButton, 2, 1)
        layout.addWidget(self.buttonBox, 2, 3, 1, 3)
        self.setLayout(layout)

        self.editor.textChanged.connect(self.updateUi)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'Insert text'))
        self.noUpdateUi = False
        self.updateUi()

    def updateUi(self):
        if self.noUpdateUi:
            return
        self.editor.document().setDefaultFont(self.font)
        self.editor.setTextColor(self.color)
        self.buttonBox.button(
            QtWidgets.QDialogButtonBox.Ok).setEnabled(
                self.editor.toPlainText() != '')

    def accept(self):
        self.graphicsTextItem.setPlainText(self.editor.toPlainText())
        self.graphicsTextItem.setFont(self.font)
        self.graphicsTextItem.update()
        QtWidgets.QDialog.accept(self)

    def chooseFont(self):
        """
        Dialogue pour choisir la police d'écriture
        """
        font, ok = QtWidgets.QFontDialog.getFont(self.font, self)
        if ok:
            self.font = font
            self.editor.document().setDefaultFont(font)
            self.graphicsTextItem.setFont(font)
            self.parent.font = font

    def penColor(self):
        """
        Dialog de sélection de couleur
        """
        self.noUpdateUi = True
        text = self.editor.toPlainText()
        newColor = QtWidgets.QColorDialog.getColor(
            self.color, 
            self, 
            QtWidgets.QApplication.translate('main', 'Select color'), 
            QtWidgets.QColorDialog.ShowAlphaChannel)
        if newColor.isValid():
            self.color = newColor
            self.editor.setPlainText('')
            self.editor.setTextColor(self.color)
            self.editor.setPlainText(text)
            self.graphicsTextItem.setDefaultTextColor(self.color)
            self.parent.fontColor = self.color
        self.noUpdateUi = False



###########################################################"
#   LA FENÊTRE D'OUTILS
###########################################################"

def createActions(self, toolsWindow):
    """
    Création des actions dans une fonction à part car elles sont
    partagées entre les 2 boîtes à outils (normale et kids)
    """

    # Les actions de base
    self.actionFileMenu = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'File'), 
        self, 
        icon=utils.doIcon('application-menu'))

    self.actionFileNew = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'New'), 
        self, 
        icon=utils.doIcon('document-new'), 
        objectName='FileNew', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.New))
    self.actionFileNew.triggered.connect(self.main.fileNew)

    self.actionFileOpen = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Open...'), 
        self, 
        icon=utils.doIcon('document-open'), 
        objectName='FileOpen', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Open))
    self.actionFileOpen.triggered.connect(self.main.fileOpen)

    self.actionFileReload = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Reload'), 
        self, 
        icon=utils.doIcon('view-refresh'), 
        objectName='FileReload', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Refresh))
    self.actionFileReload.triggered.connect(self.main.fileReload)

    self.actionFileGoNext = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Next file'), 
        self, 
        icon=utils.doIcon('go-next'), 
        objectName='FileGoNext', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Forward))
    self.actionFileGoNext.triggered.connect(self.main.fileGoNext)

    self.actionFileGoPrevious = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Previous file'), 
        self, 
        icon=utils.doIcon('go-previous'), 
        objectName='FileGoPrevious', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Back))
    self.actionFileGoPrevious.triggered.connect(self.main.fileGoPrevious)

    self.actionFileSave = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Save'), 
        self, 
        icon=utils.doIcon('document-save'), 
        objectName='FileSave', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Save))
    self.actionFileSave.triggered.connect(self.main.fileSave)

    self.actionFileSaveAs = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Save as...'), 
        self, 
        icon=utils.doIcon('document-save-as'), 
        objectName='FileSaveAs', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.SaveAs))
    self.actionFileSaveAs.triggered.connect(self.main.fileSaveAs)

    """
    self.actionPrint = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Print'), 
        self, 
        icon=utils.doIcon('document-print'), 
        objectName='Print', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Print))
    self.actionPrint.triggered.connect(self.main.documentPrint)
    """

    """
    self.actionExportPdf = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Export PDF'), 
        self, 
        icon=utils.doIcon('application-pdf'), 
        objectName='ExportPdf')
    self.actionExportPdf.triggered.connect(self.main.exportPdf)
    """

    self.actionExportSVG = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Export SVG'), 
        self, 
        icon=utils.doIcon('image-x-svg+xml'), 
        objectName='ExportSVG')
    self.actionExportSVG.triggered.connect(self.main.exportSVG)

    self.actionExportPNG = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Export PNG'), 
        self, 
        icon=utils.doIcon('image-png'), 
        objectName='ExportPNG')
    self.actionExportPNG.triggered.connect(self.main.exportPNG)

    self.actionConfigure = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Configure'), 
        self, 
        icon=utils.doIcon('configure'), 
        objectName='Configure', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Preferences))
    self.actionConfigure.triggered.connect(self.main.configure)

    self.actionMinimize = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Minimize'), 
        self, 
        icon=utils.doIcon('go-down'), 
        objectName='Minimize')
    self.actionMinimize.triggered.connect(self.main.showMinimized)

    self.actionCreateLinuxLauncher = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Create a launcher'), 
        self, 
        icon=utils.doIcon('logo_linux'), 
        objectName='CreateLinuxLauncher')
    self.actionCreateLinuxLauncher.triggered.connect(
        self.main.createLinuxLauncher)


    # Les actions des screenshots et autres fonds
    self.actionNewScreenshot2 = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'New screenshot'), 
        self, 
        icon=utils.doIcon('camera-photo'), 
        objectName='NewScreenshot2')
    self.actionNewScreenshot2.triggered.connect(self.main.newScreenshot)

    self.actionTransparentArea = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Transparent area'), 
        self, 
        icon=utils.doIcon('background-transparent'), 
        objectName='TransparentArea')
    self.actionTransparentArea.triggered.connect(self.main.transparentArea)

    self.actionWhitePage = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'White page'), 
        self, 
        icon=utils.doIcon('background-white'), 
        objectName='WhitePage')
    self.actionWhitePage.triggered.connect(self.main.whitePage)

    self.actionPointsPage = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Dotted paper'), 
        self, 
        icon=utils.doIcon('background-points'), 
        objectName='PointsPage')
    self.actionPointsPage.triggered.connect(self.main.pointsPage)

    self.actionGridPage = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Grid'), 
        self, 
        icon=utils.doIcon('background-grid'), 
        objectName='GridPage')
    self.actionGridPage.triggered.connect(self.main.gridPage)

    self.actionBackGround = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Choose background'), 
        self, 
        icon=utils.doIcon('choose-background'), 
        objectName='ChooseBackground')
    self.actionBackGround.triggered.connect(self.main.chooseBackGround)


    # Les actions des instruments
    self.actionShowRuler = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Ruler'), 
        self, 
        icon=utils.doIcon('instrument-ruler'), 
        objectName='ShowRuler', 
        checkable=True, 
        checked=False)
    self.actionShowRuler.triggered.connect(toolsWindow.showInstrument)

    self.actionShowRulerNotGraduated = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Ruler (not graduated)'), 
        self, 
        icon=utils.doIcon('instrument-ruler'), 
        objectName='ShowRulerNotGraduated', 
        checkable=True, 
        checked=False)
    self.actionShowRulerNotGraduated.triggered.connect(
        toolsWindow.showInstrument)

    self.actionShowSquare = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Square'), 
        self, 
        icon=utils.doIcon('instrument-square'), 
        objectName='ShowSquare', 
        checkable=True, 
        checked=False)
    self.actionShowSquare.triggered.connect(toolsWindow.showInstrument)

    self.actionShowSquareNotGraduated = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Square (not graduated)'), 
        self, 
        icon=utils.doIcon('instrument-square'), 
        objectName='ShowSquareNotGraduated', 
        checkable=True, 
        checked=False)
    self.actionShowSquareNotGraduated.triggered.connect(
        toolsWindow.showInstrument)

    self.actionShowProtractor = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Protractor'), 
        self, 
        icon=utils.doIcon('instrument-protractor'), 
        objectName='ShowProtractor', 
        checkable=True, 
        checked=False)
    self.actionShowProtractor.triggered.connect(
        toolsWindow.showInstrument)

    self.actionShowCompass = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Compass'), 
        self, 
        icon=utils.doIcon('instrument-compass'), 
        objectName='ShowCompass', 
        checkable=True, 
        checked=False)
    self.actionShowCompass.triggered.connect(
        toolsWindow.showInstrument)

    self.actionLockInstruments = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Lock instruments'), 
        self, 
        icon=utils.doIcon('object-locked'), 
        objectName='LockInstruments', 
        checkable=True, 
        checked=False)
    self.actionLockInstruments.triggered.connect(
        toolsWindow.lockInstruments)

    self.actionUnitsLock = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Lock units'), 
        self, 
        icon=utils.doIcon('units-lock'), 
        objectName='UnitsLock', 
        checkable=True, 
        checked=self.main.configDict['MAIN']['unitsLocked'])

    self.actionUnitsSave = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Save units'), 
        self, 
        icon=utils.doIcon('units-save'), 
        objectName='UnitsSave')
    self.actionUnitsSave.triggered.connect(self.main.unitsActions)

    self.actionUnitsRestore = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Restore backed up units'), 
        self, 
        icon=utils.doIcon('units-open'), 
        objectName='UnitsRestore')
    self.actionUnitsRestore.triggered.connect(self.main.unitsActions)

    self.actionUnitsInit = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Reset units'), 
        self, 
        icon=utils.doIcon('units-init'), 
        objectName='UnitsInit')
    self.actionUnitsInit.triggered.connect(self.main.unitsActions)

    self.actionShowFalseCursor = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Show false cursor'), 
        self, 
        icon=utils.doIcon('cursor'), 
        objectName='ShowFalseCursor', 
        checkable=True, 
        checked=False)
    self.actionShowFalseCursor.triggered.connect(
        toolsWindow.showFalseCursor)


    # Les actions des outils de dessin
    self.actionSelect = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Select'), 
        self, 
        icon=utils.doIcon('edit-select'), 
        objectName='Select', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.SelectAll))
    self.actionSelect.setData('SELECT')

    self.actionDrawLine = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Line'), 
        self, 
        icon=utils.doIcon('draw-line'), 
        objectName='DrawLine')
    self.actionDrawLine.setData('LINE')

    self.actionDrawCurve = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Curve'), 
        self, 
        icon=utils.doIcon('draw-freehand'), 
        objectName='DrawCurve')
    self.actionDrawCurve.setData('CURVE')

    self.actionHighlighterPen = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Highlighter'), 
        self, 
        icon=utils.doIcon('highlighter'), 
        objectName='HighlighterPen')
    self.actionHighlighterPen.setData('HIGHLIGHTER')

    self.actionAddText = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Add text'), 
        self, 
        icon=utils.doIcon('draw-text'), 
        objectName='AddText')
    self.actionAddText.setData('TEXT')

    self.actionAddPoint = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Add point'), 
        self, 
        icon=utils.doIcon('point'), 
        objectName='AddPoint')
    self.actionAddPoint.setData('POINT')

    self.actionAddPixmap = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Add pixmap'), 
        self, 
        icon=utils.doIcon('insert-image'), 
        objectName='AddPixmap')
    self.actionAddPixmap.setData('PIXMAP')

    self.actionCopy = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Copy'), 
        self, 
        icon=utils.doIcon('edit-copy'), 
        objectName='Copy', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Copy))

    self.actionPaste = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Paste'), 
        self, 
        icon=utils.doIcon('edit-paste'), 
        objectName='Paste', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Paste))

    self.actionUndo = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Undo'), 
        self, 
        icon=utils.doIcon('edit-undo'), 
        objectName='Undo', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Undo))

    self.actionRedo = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Redo'), 
        self, 
        icon=utils.doIcon('edit-redo'), 
        objectName='Redo', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Redo))

    self.actionDeleteSelected = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Delete selected object'), 
        self, 
        icon=utils.doIcon('draw-eraser'), 
        objectName='Delete', 
        shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Delete))

    self.actionEraseAll = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Erase all'), 
        self, 
        icon=utils.doIcon('edit-delete'), 
        objectName='Erase')


    # Les actions des couleurs
    self.actionCustomColors = []
    for i in range(10):
        actionName = 'CustomColor{0}'.format(i)
        action = QtWidgets.QAction('', self, objectName=actionName)
        action.triggered.connect(self.main.view.selectColor)
        color = self.main.configDict['COLORS'][actionName]
        color = QtGui.QColor(color[0], color[1], color[2], color[3])
        pixmap = QtGui.QPixmap(
            utils.STYLE['PM_LargeIconSize'], 
            utils.STYLE['PM_LargeIconSize'])
        pixmap.fill(color)
        action.setIcon(QtGui.QIcon(pixmap))
        action.setData(color)
        self.actionCustomColors.append(action)


    # Les actions des tailles
    self.actionCustomWidths = []
    for i in range(5):
        actionName = 'CustomWidth{0}'.format(i)
        action = QtWidgets.QAction('', self, objectName=actionName)
        action.triggered.connect(self.main.view.selectWidth)
        width = self.main.configDict['WIDTHS'][actionName]
        action.setIcon(utils.doIcon('custom-width-{0}'.format(i)))
        action.setText('{0}'.format(width))
        action.setData(width)
        self.actionCustomWidths.append(action)


    # Les actions des styles
    self.actionPenStyleSolid = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Solid'), 
        self, 
        icon=utils.doIcon('line-style-solid'), 
        objectName='Solid')
    self.actionPenStyleSolid.triggered.connect(self.main.view.setPenStyle)

    self.actionPenStyleDash = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Dash'), 
        self, 
        icon=utils.doIcon('line-style-dash'), 
        objectName='Dash')
    self.actionPenStyleDash.triggered.connect(self.main.view.setPenStyle)

    self.actionPenStyleDot = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Dot'), 
        self, 
        icon=utils.doIcon('line-style-dot'), 
        objectName='Dot')
    self.actionPenStyleDot.triggered.connect(self.main.view.setPenStyle)

    self.actionPenStyleDashDot = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Dash Dot'), 
        self, 
        icon=utils.doIcon('line-style-dashdot'), 
        objectName='DashDot')
    self.actionPenStyleDashDot.triggered.connect(self.main.view.setPenStyle)

    self.actionPenStyleDashDotDot = QtWidgets.QAction(
        QtWidgets.QApplication.translate('main', 'Dash Dot Dot'), 
        self, 
        icon=utils.doIcon('line-style-dashdotdot'), 
        objectName='DashDotDot')
    self.actionPenStyleDashDotDot.triggered.connect(
        self.main.view.setPenStyle)



class ToolsWindow(QtWidgets.QMainWindow):
    """
    La boîte à outils.
    Elle contient les différentes barres d'outils,
        et les actions associées.
    """
    def __init__(self, parent):
        super(ToolsWindow, self).__init__(parent)
        """
        Mise en place. On crée les actions, les barres d'outils, ...
        """
        self.main = parent
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'Tools'))
        self.toolsKidMode = False
        self.defaultFlags = (
            self.windowFlags() | 
            QtCore.Qt.Tool | 
            QtCore.Qt.WindowStaysOnTopHint)
        self.kidFlags = (
            QtCore.Qt.Tool | 
            QtCore.Qt.WindowTitleHint | 
            QtCore.Qt.CustomizeWindowHint)
        self.setWindowFlags(self.defaultFlags)

        self.actionsState = {}
        createActions(self, self)
        self.createToolBar()

    def closeEvent(self, event):
        self.main.quit()

    def keyPressEvent(self, event):
        if event.matches(QtGui.QKeySequence.Quit):
            self.main.quit()
        elif event.matches(QtGui.QKeySequence.New):
            self.main.fileNew()
        elif event.matches(QtGui.QKeySequence.Open):
            self.main.fileOpen()
        elif event.matches(QtGui.QKeySequence.Refresh):
            self.main.fileReload()
        elif event.matches(QtGui.QKeySequence.Forward):
            self.main.fileGoNext()
        elif event.matches(QtGui.QKeySequence.Back):
            self.main.fileGoPrevious()
        elif event.matches(QtGui.QKeySequence.Save):
            self.main.fileSave()
        elif event.matches(QtGui.QKeySequence.SaveAs):
            self.main.fileSaveAs()
            """elif event.matches(QtGui.QKeySequence.Print):
            self.main.documentPrint()
            """
        elif event.matches(QtGui.QKeySequence.Preferences):
            self.main.configure()
        elif event.matches(QtGui.QKeySequence.HelpContents):
            self.main.helpPage()
        elif event.matches(QtGui.QKeySequence.SelectAll):
            self.draw()
        elif event.matches(QtGui.QKeySequence.Copy):
            self.main.edit(what='COPY')
        elif event.matches(QtGui.QKeySequence.Paste):
            self.main.edit(what='PASTE')
        elif event.matches(QtGui.QKeySequence.Undo):
            self.main.edit(what='UNDO')
        elif event.matches(QtGui.QKeySequence.Redo):
            self.main.edit(what='REDO')
        elif event.matches(QtGui.QKeySequence.Delete):
            self.main.edit(what='DELETE')
        elif event.key() == QtCore.Qt.Key_K:
            self.main.switchKidMode()

    def createToolBar(self):

        self.toolBar = QtWidgets.QToolBar()
        self.toolBar.setObjectName('toolBar')
        self.toolBar.setIconSize(
            QtCore.QSize(
                self.main.configDict['TOLLBAR']['iconSize'], 
                self.main.configDict['TOLLBAR']['iconSize']))
        #self.addToolBar(QtCore.Qt.LeftToolBarArea, self.toolBar)
        self.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)

        self.baseMenu = QtWidgets.QMenu('', self)
        self.actionFileMenu.setMenu(self.baseMenu)
        self.actionFileMenu.triggered.connect(self.fileMenu)

        self.lastFilesMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', 'Recent Files'), 
            self)
        self.lastFilesMenu.setIcon(
            utils.doIcon('document-open-recent'))

        self.backgroundMenu = QtWidgets.QMenu('', self)
        self.actionNewScreenshot2.setMenu(self.backgroundMenu)

        self.actionInstruments = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Geometry instruments'), 
            self, 
            icon=utils.doIcon('instruments'))
        self.instrumentsMenu = QtWidgets.QMenu('', self)
        self.actionInstruments.setMenu(self.instrumentsMenu)

        self.actionDraw = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Select'), 
            self, 
            icon=utils.doIcon('edit-select'), 
            checkable=True, 
            checked=True)
        self.actionDraw.setData('SELECT')
        self.actionDraw.triggered.connect(self.draw)
        self.drawMenu = QtWidgets.QMenu('', self)
        self.actionDraw.setMenu(self.drawMenu)

        textEdit = QtWidgets.QApplication.translate('main', 'click to edit')

        text = utils_functions.u('{0} ({1})').format(
            QtWidgets.QApplication.translate('main', 'Colors'), 
            textEdit)
        self.actionColors = QtWidgets.QAction(
            text, 
            self)
        self.colorsMenu = QtWidgets.QMenu('', self)
        self.actionColors.setMenu(self.colorsMenu)
        pixmap = QtGui.QPixmap(
            utils.STYLE['PM_LargeIconSize'], 
            utils.STYLE['PM_LargeIconSize'])
        pixmap.fill(self.main.view.drawPen.color())
        self.actionColors.setIcon(QtGui.QIcon(pixmap))
        self.actionColors.triggered.connect(self.main.view.editColor)

        text = utils_functions.u('{0} {1} ({2})').format(
            QtWidgets.QApplication.translate('main', 'Width:'), 
            self.main.view.drawPen.width(), 
            textEdit)
        self.actionWidths = QtWidgets.QAction(
            text, 
            self, 
            icon=utils.doIcon('custom-width-1'))
        self.widthsMenu = QtWidgets.QMenu('', self)
        self.actionWidths.setMenu(self.widthsMenu)
        self.actionWidths.triggered.connect(self.main.view.editWidth)

        self.actionPenStyle = QtWidgets.QAction(self)
        self.stylesMenu = QtWidgets.QMenu('', self)
        self.actionPenStyle.setMenu(self.stylesMenu)

        self.actions = {
            'LIST': (
                (self.baseMenu, 'base'), 
                (self.backgroundMenu, 'background'), 
                (self.instrumentsMenu, 'instruments'), 
                (self.drawMenu, 'draw'), 
                (self.colorsMenu, 'colors'), 
                (self.widthsMenu, 'widths'), 
                (self.stylesMenu, 'styles'), 
                ), 
            'TITLE': {
                self.baseMenu: QtWidgets.QApplication.translate(
                    'main', 'Basic tools'), 
                self.backgroundMenu: QtWidgets.QApplication.translate(
                    'main', 'Screenshots and backgrounds'), 
                self.drawMenu: QtWidgets.QApplication.translate(
                    'main', 'Drawing tools'), 
                self.colorsMenu: QtWidgets.QApplication.translate(
                    'main', 'Colors'), 
                self.widthsMenu: QtWidgets.QApplication.translate(
                    'main', 'Widths'), 
                self.stylesMenu: QtWidgets.QApplication.translate(
                    'main', 'Styles'), 
                }, 

            # Outils de base (quitter, aide, ...)
            'base': (
                self.actionFileNew, 
                self.actionFileOpen, 
                'LASTFILES', 
                self.actionFileReload, 
                self.actionFileGoNext, 
                self.actionFileGoPrevious, 
                'SEPARATOR', 
                self.actionFileSave, 
                self.actionFileSaveAs, 
                #self.actionPrint, 
                #self.actionExportPdf, 
                self.actionExportSVG, 
                self.actionExportPNG, 
                'SEPARATOR', 
                self.actionConfigure, 
                self.actionMinimize, 
                self.actionCreateLinuxLauncher, 
                'SEPARATOR', 
                self.main.actionHelp, 
                self.main.actionAbout, 
                'SEPARATOR', 
                self.main.actionQuit, 
                ), 
            # Photos d'écran et fonds
            'background': (
                self.main.actionNewScreenshot, 
                self.actionTransparentArea, 
                self.actionWhitePage, 
                self.actionPointsPage, 
                self.actionGridPage, 
                self.actionBackGround, 
                ), 
            # Instruments de géométrie
            'instruments': (
                self.actionShowRuler, 
                self.actionShowSquare, 
                self.actionShowProtractor, 
                self.actionShowCompass, 
                self.actionShowRulerNotGraduated, 
                self.actionShowSquareNotGraduated, 
                'SEPARATOR', 
                self.actionLockInstruments, 
                'SEPARATOR', 
                self.actionUnitsLock, 
                self.actionUnitsSave, 
                self.actionUnitsRestore, 
                self.actionUnitsInit, 
                'SEPARATOR', 
                self.actionShowFalseCursor, 
                ), 
            # Outils de dessin
            'draw': (
                self.actionSelect, 
                'SEPARATOR', 
                #self.actionPens, 
                self.actionDrawLine, 
                self.actionDrawCurve, 
                self.actionHighlighterPen, 
                'SEPARATOR', 
                self.actionAddText, 
                self.actionAddPoint, 
                self.actionAddPixmap, 
                'SEPARATOR', 
                self.actionCopy, 
                self.actionPaste, 
                self.actionUndo, 
                self.actionRedo, 
                self.actionDeleteSelected, 
                self.actionEraseAll, 
                ), 
            # les couleurs persos
            'colors': (
                self.actionCustomColors[0], 
                self.actionCustomColors[1], 
                self.actionCustomColors[2], 
                self.actionCustomColors[3], 
                self.actionCustomColors[4], 
                self.actionCustomColors[5], 
                self.actionCustomColors[6], 
                self.actionCustomColors[7], 
                self.actionCustomColors[8], 
                self.actionCustomColors[9], 
                #'SEPARATOR', 
                #self.actionEditColor, 
                ), 
            # les tailles persos
            'widths': (
                self.actionCustomWidths[0], 
                self.actionCustomWidths[1], 
                self.actionCustomWidths[2], 
                self.actionCustomWidths[3], 
                self.actionCustomWidths[4], 
                ), 
            # les styles
            'styles': (
                self.actionPenStyleSolid, 
                self.actionPenStyleDash, 
                self.actionPenStyleDot, 
                self.actionPenStyleDashDot, 
                self.actionPenStyleDashDotDot, 
                ), 
            }

        for (menu, actions) in self.actions['LIST']:
            for action in self.actions[actions]:
                if action == 'SEPARATOR':
                    menu.addSeparator()
                elif action == 'LASTFILES':
                    menu.addMenu(self.lastFilesMenu)
                else:
                    if actions == 'draw':
                        action.triggered.connect(self.draw)
                    menu.addAction(action)
                    actionName = action.objectName()
                    actionText = action.text()
                    if action in self.actionCustomColors:
                        actionText = utils_functions.u('{0} {1}').format(
                            QtWidgets.QApplication.translate(
                                'main', 'Custom color'), 
                            self.actionCustomColors.index(action) + 1)
                    if action in self.actionCustomWidths:
                        actionText = utils_functions.u('{0} {1}').format(
                            QtWidgets.QApplication.translate(
                                'main', 'Custom width'), 
                            self.actionCustomWidths.index(action) + 1)

                    visible = not(
                        actionName in self.main.configDict['TOLLBAR']['masked'])
                    action.setVisible(visible)
                    kidVisible = (
                        actionName in self.main.configDict['KID']['visible'])

                    self.actionsState[action] = [
                        actionText, 
                        action.isVisible(), 
                        kidVisible]

        self.reloadToolBar()

    def reloadToolBar(self):
        """
        Si on modifie la taille de icônes, il faut mettre à jour l'affichage
        """
        if self.toolsKidMode:
            self.toolBar.clear()
            self.toolBar.setIconSize(
                QtCore.QSize(
                    self.main.configDict['KID']['iconSize'], 
                    self.main.configDict['KID']['iconSize']))
            self.toolBar.setFloatable(False)
            for (menu, actions) in self.actions['LIST']:
                for action in self.actions[actions]:
                    if action in self.actionsState:
                        if self.actionsState[action][2]:
                            self.toolBar.addAction(action)
        else:
            self.toolBar.clear()
            self.toolBar.setIconSize(
                QtCore.QSize(
                    self.main.configDict['TOLLBAR']['iconSize'], 
                    self.main.configDict['TOLLBAR']['iconSize']))
            self.toolBar.addAction(self.actionFileMenu)
            self.toolBar.addAction(self.actionNewScreenshot2)
            self.toolBar.addAction(self.actionInstruments)
            self.toolBar.addAction(self.actionDraw)
            self.toolBar.addAction(self.actionColors)
            self.toolBar.addAction(self.actionWidths)
            self.toolBar.addAction(self.actionPenStyle)

    def showInstrument(self):
        """
        affiche ou masque la règle
        """
        if self.sender() == self.actionShowRuler:
            WithRuler = self.actionShowRuler.isChecked()
            self.main.ruler.setVisible(WithRuler)
        elif self.sender() == self.actionShowRulerNotGraduated:
            WithRulerNotGraduated = self.actionShowRulerNotGraduated.isChecked()
            self.main.rulerNotGraduated.setVisible(WithRulerNotGraduated)
        elif self.sender() == self.actionShowSquare:
            WithSquare = self.actionShowSquare.isChecked()
            self.main.square.setVisible(WithSquare)
        elif self.sender() == self.actionShowSquareNotGraduated:
            WithSquareNotGraduated = self.actionShowSquareNotGraduated.isChecked()
            self.main.squareNotGraduated.setVisible(WithSquareNotGraduated)
        elif self.sender() == self.actionShowProtractor:
            WithProtractor = self.actionShowProtractor.isChecked()
            self.main.protractor.setVisible(WithProtractor)
        elif self.sender() == self.actionShowCompass:
            WithCompass = self.actionShowCompass.isChecked()
            self.main.compass.setVisible(WithCompass)

    def lockInstruments(self):
        """
        Bloque ou débloque l'utilisation des instruments
        """
        self.main.view.state['locked'] = self.actionLockInstruments.isChecked()
        for instrument in self.main.listeInstruments:
            if self.main.view.state['locked']:
                utils.doFlags(instrument, 'locked')
            else:
                utils.doFlags(instrument, 'movable')

    def showFalseCursor(self):
        """
        On utilise ou pas le faux curseur.
        """
        utils_instruments.changeFalseCursor(
            self.actionShowFalseCursor.isChecked())
        self.main.myCursor.setVisible(utils_instruments.WITH_FALSE_CURSOR)
        if utils_instruments.WITH_FALSE_CURSOR:
            # on ne doit plus non plus pouvoir sélectionner les items créés :
            for item in self.main.view.scene().items():
                if not(item in self.main.listeInstruments):
                    utils.doFlags(item, 'ignoresTransformations')
            # on remet le curseur par défaut :
            self.main.view.changeCursor()
            self.main.drawingMode = 'NO'
        else:
            self.main.view.changeCursor()

    def draw(self):
        """
        """
        actionDraw = False
        sender = self.sender()
        if sender == self.actionDraw:
            actionDraw = True
        elif sender == None:
            sender = self.actionSelect
            actionDraw = False
            self.actionDraw.setChecked(True)
        elif len(sender.objectName()) < 1:
            actionDraw = True
            self.actionDraw.setChecked(True)
        elif sender.data() == None:
            self.main.edit(what=sender.objectName().upper())
            return
        if actionDraw:
            checked = self.actionDraw.isChecked()
            if checked:
                self.main.drawingMode = self.actionDraw.data()
                if self.main.drawingMode == 'SELECT':
                    for item in self.main.view.scene().items():
                        if not(item in self.main.listeInstruments):
                            utils.doFlags(item, 'selectable')
                else:
                    self.main.view.changeCursor('CROSS')
            else:
                # on ne doit plus non plus pouvoir 
                # sélectionner les items créés :
                for item in self.main.view.scene().items():
                    if not(item in self.main.listeInstruments):
                        utils.doFlags(item, 'ignoresTransformations')
                # on remet le curseur par défaut :
                self.main.view.changeCursor()
                self.main.drawingMode = 'NO'
        else:
            self.actionDraw.setIcon(sender.icon())
            self.actionDraw.setData(sender.data())
            self.actionDraw.setText(sender.text())
            self.actionDraw.setChecked(True)

            if self.actionDraw.isChecked():
                self.main.drawingMode = sender.data()
                if self.main.drawingMode == 'SELECT':
                    for item in self.main.view.scene().items():
                        if not(item in self.main.listeInstruments):
                            utils.doFlags(item, 'selectable')
                    # on remet le curseur par défaut :
                    self.main.view.changeCursor()
                else:
                    self.main.view.changeCursor('CROSS')
            else:
                # on ne doit plus non plus pouvoir 
                # sélectionner les items créés :
                for item in self.main.view.scene().items():
                    if not(item in self.main.listeInstruments):
                        utils.doFlags(item, 'ignoresTransformations')
                # on remet le curseur par défaut :
                self.main.view.changeCursor()
                self.main.drawingMode = 'NO'

    def fileMenu(self):
        self.actionFileMenu.menu().popup(QtGui.QCursor.pos())


